#include <iostream>

using namespace std;

const int tamanho=10;
int* vetor[tamanho];

void inicializandoVetor(int* vet[], int tam){
    for (int i = 0; i < tam; i++){
        vet[i] = new int;
        *(vet[i]) = 0;
    }
}

void lerValoresVetor(int* vet[], int tam){
    for (int i = 0; i < tam; i++){
        cout << "Posicao " << (i + 1) << ": ";
        cin >> *(vet[i]);
    }
}

void imprimirValores(int* vet[], int tam){
    for (int i = 0; i < tam; i++){
        cout << "Posicao " << (i + 1)<< ": " << *(vet[i])<< endl;
    }
    cout << "Fim do Programa!!" << endl;
}

int main()
{
    inicializandoVetor(vetor, tamanho);
    lerValoresVetor(vetor, tamanho);
    imprimirValores(vetor, tamanho);
}
