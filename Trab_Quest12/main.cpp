#include <iostream>
#include <stack>

using namespace std;

const int tamanho = 10;
stack<string> pilha;
char caracter;

void criarPilha(int tam){
    if(pilha.empty()){
        for(int i=0; i < tam; i++){
            cout << "Insira um caracter o caracter da posicao: " << (i+1) << endl;
            cin >> caracter;
            pilha.push(&caracter);
        }
    }else{
        cout << "A pilha esta cheia!";
    }
}

void imprimirPilha(int tam){
    for(int i=0; i < tam; i++){
        cout << "Os caracteres da pilha invertidos sao: " << pilha.top() << endl;
        pilha.pop();
    }
}

int main() {
    criarPilha(tamanho);
    imprimirPilha(tamanho);
}
