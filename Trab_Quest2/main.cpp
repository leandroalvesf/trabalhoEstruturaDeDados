#include <iostream>

using namespace std;

const int tamanho = 3;

int matriz [tamanho][tamanho];

void criarMatriz(int tam, int mt[tamanho][tamanho]){

    for (int i = 0;i < tam; i++){
        cout << "Linha: " << (i + 1) << endl;
        for(int j = 0; j < tam; j++){
            cin >> mt[i][j];
        }
    }
}

void compararValores(int tam, int mt[tamanho][tamanho]){
    int menor;
    for (int i = 0;i < tam; i++){
        for(int j = 0; j < tam; j++){
            if(menor > mt[i][j]){
            menor = mt[i][j];
            }
        }
   }
 cout << "O menor valor da matriz e: " << menor;
}

int main()
{
    criarMatriz(tamanho, matriz);
    compararValores(tamanho, matriz);
}
