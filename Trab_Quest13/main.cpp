#include <iostream>

using namespace std;

int **vetor;
int tamanho;

int** inicializar(int tam){
    int** vet = new int*[tam];
    for (int i=0; i < tam;i++){
        vet[i]=new int;
    }
    return vet;
}

void lerVetor(int *vet[],int tam){
    for (int i=0; i< tam; i++){
        if (vet[i]!= nullptr){
          cout << "Posicao "<<(i+1)<<": ";
          cin >> *(vet[i]);
        }
    }
}

void bublleSort(int *vet[],int tam){
	for (int i=tam-1; i>=0; i--) {
		for (int j=0; j<i; j++) {
			if (*vet[j] > *vet [j+1]){
				int temp = *vet[j];
				*vet [j] = *vet [j+1];
				*vet [j+1]=temp;
			}
		}
	}
}

void imprimirVetor(int **vet,int tam){
    cout<<endl<<endl<<"Valores do vetor: "<<endl;
    for (int i=0; i< tam; i++){
        if (vet[i]!=NULL){
            cout<<"Posicao["<<(i+1)<<"]: "<<*(vet[i])<<endl;
        } else {
            cout<<"Posicao["<<(i+1)<<"]: "<<"null"<<endl;
        }
    }
}

int main()
{
    cout <<"Digite o tamanho do vetor: ";
    cin >> tamanho;
    vetor=inicializar(tamanho);
    lerVetor(vetor,tamanho);
    bublleSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);

    return 0;
}
