#include <iostream>

using namespace std;

const int tamanho = 10;

int vetor[tamanho];

void lerVetor(int vet[],int tam){

    for (int i=0;i<tam;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[],int tam){

     cout << "A posicao do vetor ordenado pelo bubble sort eh:" << endl;

    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
    }
}

void bubbleSort(int vet[],int tam){
    for(int i=0; i < tam; i++){
        for(int j = tam-1; j >= 1 ;j--){
            if(vet[j-1] > vet[j]){
                int temp = vet[j-1];
                vet[j-1] = vet[j];
                vet[j] = temp;
            }
        }
    }
}

int main()
{
    lerVetor(vetor,tamanho);
    bubbleSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);
}
