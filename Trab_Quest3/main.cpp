#include <iostream>

using namespace std;

const int tamanho = 3;

int matriz1[tamanho][tamanho];
int matriz2[tamanho][tamanho];
int soma[tamanho][tamanho];
int multi[tamanho][tamanho];
int i;
int j;

void popularMatriz(int mt1[tamanho][tamanho], int mt2[tamanho][tamanho], int tam){
        for(i = 0;i < tam;i++){
        cout << "Matriz 1" << endl;
        cout << "Linha: " << (i+1) << endl;
        for(j = 0;j < tam;j++){
           cin >> mt1[i][j];
        }
    }

    for(i = 0;i < tam;i++){
        cout << "Matriz 2" << endl;
        cout << "Linha: " << (i+1) << endl;
        for(j = 0;j < tam;j++){
             cin >> mt2[i][j];
        }
    }
}

void calcularMatriz(int mt1[tamanho][tamanho], int mt2[tamanho][tamanho], int tam){
      cout << "\nMATRIZ 1:" << endl;
      for(i = 0;i < tam;i++){
        for(j = 0;j < tam;j++){
             cout << mt1[i][j] << " ";
        }
        cout << endl;
      }

      cout << "\nMATRIZ 2:" << endl;
      for(i = 0;i < tam;i++){
        for(j = 0;j < tam;j++){
             cout << mt2[i][j] << " ";
         }
        cout << endl;
      }

      cout << "\nSoma das matrizes:" << endl;
      for(i = 0;i < tam;i++){
        for(j = 0;j < tam;j++){
             soma[i][j] = mt1[i][j] + mt2[i][j];
             cout << soma[i][j] << " ";
        }
        cout << endl;
      }

      cout << "\nMultiplicacao das matrizes:" << endl;
      for(i = 0;i < tam;i++){
        for(j = 0;j < tam;j++){
             multi[i][j] = mt1[i][j] * mt2[i][j];
             cout << multi[i][j] << " ";
        }
        cout << endl;
      }
}

int main()
{
    popularMatriz(matriz1, matriz2, tamanho);
    calcularMatriz(matriz1, matriz2, tamanho);
}
