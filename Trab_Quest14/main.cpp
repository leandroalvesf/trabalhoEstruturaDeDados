#include <iostream>

using namespace std;

const int tamanho = 10;

int vetor[tamanho];

void lerVetor(int vet[], int tam){
    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": ";
        cin >> vet[i];
    }
}

void imprimirVetor(int vet[], int tam){
     cout << "Ordenando o vetor usando o quick sort:" << endl;

    for(int i =0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
    }
}

void quickSort(int vet[], int esq, int dir){
    int i = esq;
    int j = dir;
    int tmp;
    int aux = vet[(esq + dir)/2];
//Divide, a partir do aux - direita e esquerda
    while (i <= j){
        while (vet[i] < aux)
            i++;
        while(vet[j] > aux)
            j--;
        if(i <= j){
            tmp = vet[i];
            vet[i] = vet[j];
            vet[j] = tmp;
            i++;
            j--;
        }
    }
   //Recursivo
    if(esq < j){
        quickSort(vet, esq, j);
    }
    if(i < dir){
        quickSort(vet, i, dir);
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    quickSort(vetor,0,tamanho-1);
    imprimirVetor(vetor,tamanho);
}
