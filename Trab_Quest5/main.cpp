#include <iostream>
#include <stdlib.h>

using namespace std;

const int tamanho = 3;

int i;

struct registro {
        int codigo;
        char nome[60];
        char endereco[60];
};

struct registro reg[tamanho];

void incrementarRegistro (int tam){
    for(i = 0; i < tam; i++){
    cout << "Insira o codigo: " << endl;
    cin >> reg[i].codigo;
    cout << "Insira o nome: " << endl;
    cin >> reg[i].nome;
    cout << "Insira o endereco: " << endl;
    cin >> reg[i].endereco;
    cout << "-------------------" << endl;
    }
}

void pesquisarStruct(int cod, int tam){
    for(i = 0;i < tam;i++){
        if(cod == reg[i].codigo){
            cout << "O nome eh: " << reg[i].nome << "." << endl;
            cout << "O endereco eh: " << reg[i].endereco << "." << endl;
            exit(0);
        }
    }
    cout << "Codigo nao encontrado, insira um codigo valido para efetuar a pesquisa." << endl;
}

int main()
{
    int cod;

    incrementarRegistro(tamanho);
    cout << "Insira o numero do codigo do registro que deseja pesquisar: " << endl;
    cin >> cod;
    pesquisarStruct(cod, tamanho);
}
