#include <iostream>

using namespace std;

const int tamanho = 15;

int* vetor[tamanho];
int i;
int j;
int aux;

void inicializarVetor(int* vet[], int tam){

    for(i=0; i < tam; i++){
        cout << "Insira um valor para a posicao " << (i+1) << ": " << endl;
        vet[i] = new int[i];
        cin >> *(vet[i]);
    }
}

void imprimirVetor(int* vet[], int tam){

    for ( i = tam - 1; i >= 0; i--){
        cout << "O valor do vetor invertidos usando ponteiro eh: " << *(vet[i]) << endl;
    }
}

int main()
{
inicializarVetor(vetor, tamanho);
imprimirVetor(vetor, tamanho);
}
